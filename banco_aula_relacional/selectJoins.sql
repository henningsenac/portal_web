-- SELECT * FROM cidades c INNER JOIN prefeitos p ON c.id = p.cidade_id;

-- SELECT * FROM prefeitos p INNER JOIN cidades c ON p.cidade_id = c.id ;

-- INNER JOIN 
SELECT 
    c.id AS IdCidade,  
    c.nome AS Cidade, 
    c.estado_id AS IdEstado, 
    c.area AS Area,
    p.id AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
INNER JOIN prefeitos p 
ON c.id = p.cidade_id ;

-- LEFT JOIN
SELECT 
    c.id AS IdCidade,  
    c.nome AS Cidade, 
    c.estado_id AS IdEstado, 
    c.area AS Area,
    p.id AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
LEFT JOIN prefeitos p 
ON c.id = p.cidade_id;

-- RIGHT JOIN
SELECT 
    c.id AS IdCidade,  
    c.nome AS Cidade, 
    c.estado_id AS IdEstado, 
    c.area AS Area,
    p.id AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
RIGHT JOIN prefeitos p 
ON c.id = p.cidade_id;

-- FULL JOIN 'UNION'
SELECT 
    c.id AS IdCidade,  
    c.nome AS Cidade, 
    c.estado_id AS IdEstado, 
    c.area AS Area,
    p.id AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
RIGHT JOIN prefeitos p 
ON c.id = p.cidade_id 
UNION
SELECT 
    c.id AS IdCidade,  
    c.nome AS Cidade, 
    c.estado_id AS IdEstado, 
    c.area AS Area,
    p.id AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
LEFT JOIN prefeitos p 
ON c.id = p.cidade_id;