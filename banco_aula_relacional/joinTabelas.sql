-- SELECT * FROM estados e, cidades c WHERE e.id = c.estado_id; -- select por id com nomes apelido

-- SELECT 
--     e.nome AS Estado, 
--     c.nome AS Cidade, 
--     regiao AS Região 
-- FROM estados e, cidades c 
-- WHERE e.id = c.estado_id; -- select por id com nomes apelido com outras tabelas

SELECT
    c.nome AS Cidade,
    e.nome AS Estado,
    regiao AS Região
FROM estados e
INNER JOIN cidades c ON e.id = c.estado_id; -- consulta usando o INNER para juntar tabelas