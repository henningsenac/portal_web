-- UPDATE estados SET nome = 'Maranhão' WHERE sigla = 'MA'; -- atualizando campo

-- SELECT nome FROM estados WHERE sigla = 'MA'; -- chamada pelo nome da tabela
-- SELECT `nome` FROM estados WHERE sigla = 'MA'; -- chamada por backchits
-- SELECT est.nome FROM estados est WHERE sigla = 'MA'; -- por atribuição de nome
-- SELECT est.`nome` FROM estados est WHERE sigla = 'MA'; -- chamada por atribuição de nome e backshit

UPDATE estados SET nome = 'Paraná', populacao = 11.32 WHERE sigla = 'pr';
SELECT nome, populacao FROM estados WHERE sigla = 'Pr';