-- SELECT regiao AS 'Região',
--  SUM(populacao) AS Todos
--  FROM estados GROUP BY regiao
--  ORDER BY Todos DESC;

-- SELECT regiao AS 'Região',
--  SUM(populacao) AS Todos  -- somando os valores de um campo
--  FROM estados WHERE regiao = 'Nordeste';

SELECT regiao AS 'Região',
 AVG(populacao) AS Todos  -- média os valores de um campo
 FROM estados WHERE regiao = 'Norte';