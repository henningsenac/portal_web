module.exports.form_inclui_artigo = function(app, req, res) {
    res.render("admin/form_inclui_artigo");
}

module.exports.salvar_artigo = function(app, req, res) {

    var connection = app.config.dbConnection();
		var artigosModel = new app.src.models.artigosModel(connection);

		var artigo = req.body;
		console.log(artigo);

		artigosModel.postArtigo(artigo, function (error, result) {
			res.redirect('/artigos_list');
		})

}