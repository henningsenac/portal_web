module.exports.artigos_list = function (app, req, res) {

	var connection = app.config.dbConnection();
	var artigosModel = new app.src.models.artigosModel(connection);

	artigosModel.getArtigos(function (error, result) {
		res.render("artigos/artigos_list", { artigos: result });
	});

}

module.exports.artigo_indi = function (app, req, res) {

	var connection = app.config.dbConnection();
	var artigosModel = new app.src.models.artigosModel(connection);

	artigosModel.getArtigo(function (error, result) {
		res.render("artigos/artigo_indi", { artigo: result });
	});

}