function artigosModels(connection){

	this._connection = connection;

	artigosModels.prototype.getArtigos = function(callback){
		this._connection.query('select * from artigos', callback);
	}

	artigosModels.prototype.getArtigo = function(callback){
		this._connection.query('select * from artigos where id_artigo = 1', callback);
	}
	
	artigosModels.prototype.postArtigo = function(artigo, callback){
		console.log('data: ', artigo)
		this._connection.query('insert into artigos set ?', artigo, callback);
	}
}

module.exports = function(){
	return artigosModels;
}