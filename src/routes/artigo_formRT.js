module.exports = function (app) {
	app.get('/form_inclui_artigo', function (req, res) {
		app.src.controllers.artigo_formCT.form_inclui_artigo(app, req, res)
	});

	app.post('/artigos/salvar', function (req, res) {
		app.src.controllers.artigo_formCT.salvar_artigo(app,req, res)
	});
}