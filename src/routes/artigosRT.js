module.exports = function(app) {
	
	app.get('/artigos_list', function(req, res){

		app.src.controllers.artigosCT.artigos_list(app, req, res)
	
	});

	app.get('/artigo_indi', function(req, res){

		app.src.controllers.artigosCT.artigo_indi(app, req, res)

	});

};