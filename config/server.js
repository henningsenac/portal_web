var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');

var app = express();
app.set('view engine', 'ejs');
app.set('views', './src/views');

app.use(bodyParser.urlencoded({extended: true}))

consign()
	.include('src/controllers')
	.then('src/models')
	.then('src/routes')
	.then('config/dbConnection.js')
	.into(app);

module.exports = app;